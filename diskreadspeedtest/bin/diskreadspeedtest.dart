import 'dart:io';

import 'package:args/args.dart';
import 'package:path/path.dart' as p;

void main(List<String> arguments) async {
  final parser = buildParser();
  late ArgResults args;
  try {
    args = parser.parse(arguments);
  } catch (e) {
    print('Disk Fill Speed Test');
    print(parser.usage);
    return;
  }

  final shuffle = args['shuffle'].toString() == 'true';
  final verbose = args['verbose'].toString() == 'true';
  final maxCount = int.parse(args['max-count']);
  final recursive = args['recursive'].toString() == 'true';
  final estimateIndex = args['estimate-index'].toString() == 'true';
  final baseStatsDir = args['stats-dir'];
  final runName = 'Run_${DateTime.now().millisecondsSinceEpoch}';
  final statFile = File('$baseStatsDir/stats_$runName.csv');
  final targetReadDir = args['target'];
  if (verbose) print('Reading directory structure for: $targetReadDir');
  final tmpFolder = Directory(targetReadDir);
  final files = tmpFolder
      .listSync(recursive: recursive)
      .where((e) => e.statSync().type == FileSystemEntityType.file)
      .toList();
  final length = files.length;
  final skipLength =
      maxCount == 0 || maxCount >= length ? 1 : length ~/ maxCount;
  if (shuffle) {
    files.shuffle();
  } else {
    files.sort(
        (f1, f2) => f1.statSync().changed.compareTo(f2.statSync().changed));
  }
  if (verbose) print('Sorting files by date');
  if (verbose) print('# files: ${files.length}');
  final header = 'Filename, Estimated Index, Bytes, Microseconds';
  statFile.writeAsStringSync('$header \n');
  if (verbose) print(header);
  for (var i = 0; i < length; i += skipLength) {
    final file = files[i];
    final filename = p.basename(file.path);
    final index = estimateIndex ? estimatedIndex(file.path) : 'N/A';
    final watch = Stopwatch();
    watch.start();
    final buffer = File(file.path).readAsBytesSync();
    watch.stop();
    final bufferSize = buffer.lengthInBytes;
    final statsLine =
        '$filename, $index, $bufferSize, ${watch.elapsedMicroseconds}';
    statFile.writeAsStringSync('$statsLine \n', mode: FileMode.writeOnlyAppend);
    if (verbose) print(statsLine);
  }
}

String estimatedIndex(String path) {
  const error = 'N/A';
  final filename = p.basenameWithoutExtension(path);
  final lastUnderscore = filename.lastIndexOf('_');
  if (lastUnderscore < 0) {
    return error;
  }

  final potentialNumber = filename.substring(lastUnderscore + 1);
  return int.tryParse(potentialNumber)?.toString() ?? error;
}

ArgParser buildParser() => ArgParser()
  ..addFlag(
    'verbose',
    help: 'Echo status to the command line (defaults to off)',
    negatable: true,
    defaultsTo: false,
  )
  ..addOption(
    'max-count',
    help:
        'Maximum number of data points for sampling, if not specified or zero then will get all points',
    defaultsTo: '0',
  )
  ..addFlag(
    'recursive',
    help: 'Recursively traverse the directory (defaults to not recursive)',
    negatable: true,
    defaultsTo: false,
  )
  ..addFlag(
    'shuffle',
    help:
        'Shuffle the file order to access. By default it is ordered in chronological creation order',
    negatable: true,
    defaultsTo: false,
  )
  ..addFlag(
    'estimate-index',
    help:
        'Estimate Index based on the filename, minus extension, ending with _<index> like the fill code uses',
    negatable: true,
    defaultsTo: true,
  )
  ..addOption(
    'stats-dir',
    help: 'Directory where output should go (should be different drive)',
    mandatory: true,
  )
  ..addOption(
    'target',
    help: 'Directory to write segments to',
    mandatory: true,
  );
