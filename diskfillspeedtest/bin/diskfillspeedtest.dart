import 'dart:io';

import 'package:args/args.dart';

void main(List<String> arguments) async {
  final parser = buildParser();
  late ArgResults args;
  try {
    args = parser.parse(arguments);
  } catch (e) {
    print('Disk Fill Speed Test');
    print(parser.usage);
    return;
  }
  final totalBytes = int.parse(args['max-write-kb']) * 1024;
  final fileSizeKB = int.parse(args['segment-size-kb']);
  final baseStatsDir = args['stats-dir'];
  final fillFolderBase = args['target'];
  final runName = 'Run_${DateTime.now().millisecondsSinceEpoch}';
  final statFile = File('$baseStatsDir/stats_$runName.csv');
  final sizeMB = fileSizeKB ~/ 1024;
  final size = sizeMB * 1024 * 1024;
  var bytesWritten = 0;
  var i = 0;
  if (!Directory(fillFolderBase).existsSync()) {
    Directory(fillFolderBase).createSync(recursive: true);
  }
  statFile.writeAsStringSync('File#, Bytes, Total Bytes, Microseconds\n');
  final totalTime = Stopwatch();
  totalTime.start();
  while ((totalBytes - bytesWritten) >= size) {
    i++;
    final outputPath = "$fillFolderBase/${runName}_$i.scratch";

    try {
      bytesWritten += size;
      final watch = Stopwatch();
      watch.start();
      var result = await Process.run('dd',['if=/dev/urandom', 'of=$outputPath', 'bs=1M', 'count=$sizeMB']);
      watch.stop();
      statFile.writeAsStringSync(
          '$i,$size,$bytesWritten,${watch.elapsedMicroseconds}\n',
          mode: FileMode.writeOnlyAppend);
      if(result.exitCode != 0) {
        print(result.stderr);
        break;
      }
    } catch (e) {
      print('Exception writing out data, exiting');
      break;
    }
  }

  totalTime.stop();
  print('Done!');
  print('Total execution time: ${totalTime.elapsed}');
}

ArgParser buildParser() => ArgParser()
  ..addOption(
    'segment-size-kb',
    help: 'Size of each block to write in KB',
    defaultsTo: '10',
  )
  ..addOption(
    'max-write-kb',
    help: 'Maximum number of KB to write through test',
    defaultsTo: '1024',
  )
  ..addOption(
    'stats-dir',
    help: 'Directory where output should go (should be different drive)',
    mandatory: true,
  )
  ..addOption(
    'target',
    help: 'Directory to write segments to',
    mandatory: true,
  );
